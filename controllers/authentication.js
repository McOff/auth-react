const User = require('../models/user');
const jwt = require('jwt-simple');
const config = require('../config');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user._id, iat: timestamp }, config.secret);
}

exports.signin = (req, res, next) => {
  res.send({ token: tokenForUser(req.user) });
}

exports.signup = (req, res, next) => {
  const { email, password } = req.body;
  User.findOne({ email }, (err, existingUser) => {
    if (err) return next(err);

    if(!email || !password) {
      return res.status(422).send({ error: 'You must provide emai and password'})
    }

    // If a user with email does exist, return an error
    if (existingUser) {
      return res.status(422).send({ error: 'Email is in use' });
    }

    // If a user with email does NOT exist, create and save user record
    const user = new User({
      email, password
    });

    user.save(err => {
      if (err) { return next(err); }

      //Respond to request indicating the user was created
      res.json({ token: tokenForUser(user) });
    })
  })
}
